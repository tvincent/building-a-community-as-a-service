.DEFAULT_GOAL := build
.PHONY: build clean

videos:
	mkdir videos

videos/20201122.building-a-community-as-a-service.thomas-debesse.webm: videos
	mkvmerge \
		--webm \
		--output $@ \
		--title 'Building a community as a service: how to stop suffering from “that code is meant to be forked”' \
		--default-language en \
		--global-tags metadata/matroskatags.xml \
		--chapter-charset UTF-8 \
		--chapter-language eng \
		--chapters metadata/chapters.txt \
		media/audio.opus \
		media/video.webm

build: videos/20201122.building-a-community-as-a-service.thomas-debesse.webm

clean:
	rm videos/20201122.building-a-community-as-a-service.thomas-debesse.webm
